# F-Droid Ansible Playbook

This playbook is used to pin new and updated APKs hosted on
[F-Droid](https://f-droid.org/) onto [IPFS](https://ipfs.tech/).
At the moment, it works for only Debian-based Linux distributions
and has been tested on Debian and Ubuntu installations.

## Content
* [What is this?](#what-is-this)
* [When should I use this?](#when-should-i-use-this)
* [Getting started](#getting-started)
  * [Requirements](#requirements)
  * [Installation](#installation)
  * [Inventory setup](#inventory-setup)
  * [Setting the IPFS repo Datastore.StorageMax](#setting-the-ipfs-repo-datastorestoragemax)
  * [Running the playbook](#running-the-playbook)

## What is this?

This playbook is used to pin new and updated APKs hosted on [F-Droid](https://f-droid.org/) onto [IPFS](https://ipfs.tech/).
It works by setting up a custom IPFS node, thanks to the Ansible role created by [Adam](https://codeberg.org/etam/ansible-role-ipfs) and invokes a cronjob that pins APKs once a day.

## When should I use this?

To Help F-Droid distribute its APKs via the decentralised web.

## Getting Started

Just fork this repo and help put APKs on the decentralised web.

### Requirements

* A Compute instance or Raspberry pi connected to the internet with at least 1 processor, 2GB of RAM and 10GB of Disk storage, running a Debian based linux distribution.
* A Computer with Ansible and Git installed from which the playbook will be exceuted from.

### Installation

* Use git to clone this repository to your computer

```
git clone https://gitlab.com/F-Droid/ipfs-pinning-server.git && cd ipfs-pinning-server/
```

* Install etam.ipfs role from Ansible galaxy with the command

```
ansible-galaxy install --force --role-file requirements.yml --roles-path .galaxy
```

### Inventory setup

The Ansible inventory is to be set to match the hostname intended to be the IPFS node.

This can be done as follows
Open the inventory file using your editor
Under the [ipfs] group add your node hostname(s)

`inventory`:

```
[ipfs]
# below are hostname examples
ipfs-as-a-hostname
ipfs.node.org
ubuntu@xx.xx.xx.xx
debian@yy.yy.yy.yyy
```

### Setting the IPFS repo Datastore.StorageMax

Differnet nodes/instances have different storage requirements.
Be sure to tweak this if you have more space and plan to run the node indefinitely.
This setting is the variable Datastore.StorageMax and is found on line 22 of the `debian-ipfs-setup.yml` playbook.


`debian-ipfs-setup.yml`:
```yaml
vars:
    ipfs_config_extra:
      Gateway.PublicGateways:
        localhost: null
      Datastore.StorageMax: 10GB # this is the default storage of any ipfs repo. Tweak it to suit your needs
```

### Running the playbook

After the inventory and StorageMax have been set. It is time to run the
playbook. This is done with the command

```
ansible-playbook debian-ipfs-setup.yml -i inventory
```
